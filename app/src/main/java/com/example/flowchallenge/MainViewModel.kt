package com.example.flowchallenge

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.viewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel(): ViewModel() {
    private val _postuiState = MutableStateFlow<PostUiState>(PostUiState.Loading)
    val postuiState: MutableStateFlow<PostUiState> = _postuiState
    fun  getPost(author:String) = viewModelScope.launch {
            _postuiState.value = PostUiState.Loading
            delay(3000L)
            _postuiState.value = PostUiState.Allposts(searsh(author))
    }
    private fun searsh(author: String): List<PostInfo> {
        return  Allposts.allpost.filter { author== it.author }
    }
}

