package com.example.flowchallenge

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.flowchallenge.databinding.PostLayoutBinding

class Adapter(var posts: List<PostInfo>): RecyclerView.Adapter<Adapter.PostViewHolder>() {

    inner class PostViewHolder(val binding:PostLayoutBinding ): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        Log.d("onCreateViewHolder","1")
        val view = PostLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.binding.authorName.text = posts[position].author
        holder.binding.authorImage.setImageResource(posts[position].imageId)
        holder.binding.description.text = posts[position].description


    }


    override fun getItemCount(): Int {
        return posts.size
    }
}