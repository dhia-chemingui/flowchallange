package com.example.flowchallenge
sealed class PostUiState {
    data class Allposts(val listOfPosts: List<PostInfo>) :PostUiState()
    object Loading: PostUiState()
}