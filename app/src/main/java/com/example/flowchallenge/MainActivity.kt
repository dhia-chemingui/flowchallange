package com.example.flowchallenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.example.flowchallenge.databinding.ActivityMainBinding
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager


class MainActivity : AppCompatActivity() {
    private  var _binding : ActivityMainBinding? = null
    private val binding get() = _binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.shimmerlayout.isVisible = false
        val viewModel = ViewModelProvider(owner = this).get(MainViewModel::class.java )
        binding.btnSearch.setOnClickListener(){
            val author = binding.choseAuthor.text.toString()
            binding.shimmerlayout.isVisible = true
            binding.shimmerlayout.startShimmer()
            binding.btnSearch.isVisible =false
            binding.choseAuthor.isVisible = false
            viewModel.getPost(author)
            lifecycleScope.launchWhenStarted {
                viewModel.postuiState.collect(){

                    when(it){
                        is  PostUiState.Allposts -> {
                            binding.shimmerlayout.stopShimmer()
                            binding.shimmerlayout.visibility = View.GONE
                            binding.tvPosts.visibility = View.VISIBLE
                            var adapter = Adapter(it.listOfPosts)
                            binding.tvPosts.adapter = adapter
                            binding.tvPosts.layoutManager = LinearLayoutManager(this@MainActivity)
                        }
                        else -> {Unit}
                    }
                }
            }
        }
    }
}

